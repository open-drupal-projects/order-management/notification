<?php

namespace Drupal\notification\Plugin\EmailBuilder;

use Drupal\symfony_mailer\EmailInterface;
use Drupal\symfony_mailer\Processor\EmailBuilderBase;

/**
 * Email Builder plug-in for the custom_email_example module.
 *
 * @EmailBuilder(
 *   id = "notification",
 *   sub_types = {
 *     "order_open" = @Translation("The openeing of an order"),
 *   },
 *   common_adjusters = {"email_subject", "email_body"},
 * )
 */
class NotificationBuilder extends EmailBuilderBase {

  /**
   * {@inheritDoc}
   *
   * @param \Drupal\symfony_mailer\EmailInterface $email
   *   Email Interface.
   */
  public function build(EmailInterface $email) {

  }

}
